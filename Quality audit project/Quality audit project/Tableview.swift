//
//  Tableview.swift
//  Quality audit project
//
//  Created by ShivakumarSwami on 21/06/17.
//  Copyright © 2017 studentapple. All rights reserved.
//

import UIKit

class Tableview: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var table: UITableView!
    var question = ["Accredition files","circular procedure","interaction with parents","up keep of the department units","S/N level paper presentation"]
     var marks = ["22/30","12/20","20/24","12/12","34/36"]
    var percentage = ["73.33","100","83.33","73","94.44"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func Back(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return question.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
   
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: "cell_id", for: indexPath) as! CustomPrincipalCellTableViewCell
       tableView.contentInset = UIEdgeInsets.zero
        //table.cellFrameBackground.size.height += 1;
        table.sectionHeaderHeight = 0.0;
        //table.separatorType = UITableViewCellSeparatorStyleNone
        table.separatorStyle = UITableViewCellSeparatorStyle.none
        
        cell.marks.text = marks[indexPath.row]
        cell.question.text = question[indexPath.row]
        if cell.question.text == "Accredition files"
        {
            cell.backgroundColor = UIColor.darkGray
        }else if cell.question.text == "circular procedure"
        {
            cell.backgroundColor = UIColor.black
        }else if cell.question.text == "interaction with parents"
        {
            cell.backgroundColor = UIColor.darkGray
        }else if cell.question.text == "up keep of the department units"
        {
            cell.backgroundColor = UIColor.black
        }else if cell.question.text == "S/N level paper presentation"
        {
            cell.backgroundColor = UIColor.darkGray
        }
        cell.percentage.text = percentage[indexPath.row]
        return cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
