//
//  CustomPrincipalCellTableViewCell.swift
//  Quality audit project
//
//  Created by ShivakumarSwami on 21/06/17.
//  Copyright © 2017 studentapple. All rights reserved.
//

import UIKit

class CustomPrincipalCellTableViewCell: UITableViewCell {
    @IBOutlet weak var marks: UILabel!
    @IBOutlet weak var question: UILabel!
    
    @IBOutlet weak var percentage: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
