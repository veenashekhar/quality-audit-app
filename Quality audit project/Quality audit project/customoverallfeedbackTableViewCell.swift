//
//  customoverallfeedbackTableViewCell.swift
//  Quality audit project
//
//  Created by Student on 6/21/17.
//  Copyright © 2017 studentapple. All rights reserved.
//

import UIKit

class customoverallfeedbackTableViewCell: UITableViewCell {
    

    @IBOutlet weak var department: UILabel!

    @IBOutlet weak var maxMarks: UILabel!
    
    @IBOutlet weak var marksRated: UILabel!
    
    @IBOutlet weak var percentage: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
