//
//  surveyformViewController.swift
//  Quality audit project
//
//  Created by Student on 6/21/17.
//  Copyright © 2017 studentapple. All rights reserved.
//

import UIKit

class surveyformViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var question = ["Accerediation files", "Circular procedure and maintainance"]
    var maxmarks = ["10","20"]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return question.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellid", for: indexPath) as! customsurveyformTableViewCell
        cell.question.text = question[indexPath.row]
        cell.maxMarks.text = maxmarks[indexPath.row]
        return cell
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
