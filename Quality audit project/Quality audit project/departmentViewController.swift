//
//  departmentViewController.swift
//  Quality audit project
//
//  Created by Student on 6/20/17.
//  Copyright © 2017 studentapple. All rights reserved.
//

import UIKit

class departmentViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource
{
    
    let department = ["ECE","EEE","CSE","MECHANICAL", "CIVIL","PHYSICS","CHEMISTRY"]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return department.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellid", for: indexPath) as! departmentCollectionViewCell
        
        cell.deptName.text = department[indexPath.row]
        cell.imageview.image = UIImage(named: "images-35.jpeg")
        return cell
    }

    @IBAction func homeButton(_ sender: UIButton)
    {

       _ = self.navigationController?.popToRootViewController(animated: true)
    
    }
    
}
