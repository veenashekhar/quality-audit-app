//
//  GraphViewController.swift
//  Quality audit project
//
//  Created by ShivakumarSwami on 21/06/17.
//  Copyright © 2017 studentapple. All rights reserved.
//

import UIKit

class GraphViewController: UIViewController {
    
    
    
    @IBAction func back(_ sender: Any) {
        
         self.performSegue(withIdentifier: "Go_Back_To_Table_view", sender: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
