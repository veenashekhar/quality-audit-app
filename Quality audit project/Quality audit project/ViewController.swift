//
//  ViewController.swift
//  Quality audit project
//
//  Created by Student on 6/20/17.
//  Copyright © 2017 studentapple. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    var database : FIRDatabaseReference! = nil

    // database = FIRDatabase.database().reference()
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet var mailID: UITextField!
    
        @IBOutlet weak var passWord: UITextField!
    
    @IBOutlet weak var auditor: UIButton!
    @IBOutlet weak var evaluator: UIButton!
    
    @IBOutlet weak var principal: UIButton!
    
    
    @IBOutlet weak var signup: UIButton!
    
    @IBOutlet weak var login: UIButton!
    
    
    
    @IBAction func auditor(_ sender: UIButton) {
        FIRAuth.auth()?.signIn(withEmail:self.mailID.text! , password:self.passWord.text!, completion: { (user , error) in
            
            if error != nil
            {
                let controller = UIAlertController.init(title: "error", message: error?.localizedDescription, preferredStyle: .alert)
                self.present(controller, animated: false, completion: nil)
                
                let okbutton = UIAlertAction(title: "Done", style: .destructive, handler: nil)
                controller.addAction(okbutton)
                
            }
                
            else{
                /* let controller = UIAlertController.init(title: "Note", message: "update successful,you can log in now", preferredStyle: .alert)
                 self.present(controller, animated: false, completion: nil)
                 
                 let okbutton = UIAlertAction(title: "Done", style: .destructive, handler: nil)
                 controller.addAction(okbutton)*/
                self.performSegue(withIdentifier: "toDeptVC", sender: self)
                
            }
            
            /*print(error?.localizedDescription)
             print(user?.uid)*/
            
        } )
        
            }
    
    
    @IBAction func evaluatorButton(_ sender: UIButton)
    {
        
    }
    
    
    
    @IBAction func principalButton(_ sender: UIButton)
    {
        
    }
    
    
    
    
    @IBAction func signUp(_ sender: UIButton)
    {
        
    }
    
    
    @IBAction func loginButton(_ sender: UIButton)
    {
        
    }
    
    
    @IBAction func princi(_ sender: Any) {
       
        self.performSegue(withIdentifier: "tableview123", sender: self)
    }
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        database = FIRDatabase.database().reference()

        
evaluator.layer.cornerRadius = 15
auditor.layer.cornerRadius = 15
principal.layer.cornerRadius = 15
signup.layer.cornerRadius = 15




        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

