//
//  SignUpViewController.swift
//  Quality audit project
//
//  Created by student on 22/06/17.
//  Copyright © 2017 studentapple. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {
var database = FIRDatabase.database().reference()
    @IBOutlet var userName: UITextField!
    
    @IBOutlet var department: UITextField!
    
    @IBOutlet var emailID: UITextField!
    
    @IBOutlet var password: UITextField!
    
    
    @IBAction func signUp(_ sender: UIButton) {
        
        FIRAuth.auth()?.createUser(withEmail:emailID.text!, password: password.text!, completion: { (user,error) in
            if(error != nil)
            {print("error")}
            else{
                let userref = self.database.child("User")
                let userdata = userref.child((user?.uid)!)
                let data = ["username":self.userName.text,"mailid":self.emailID.text,"department":self.department.text] as [String:Any]
                userdata.setValue(data)
                
            }
        })

        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
