//
//  overallfeedbackViewController.swift
//  Quality audit project
//
//  Created by Student on 6/21/17.
//  Copyright © 2017 studentapple. All rights reserved.
//

import UIKit

class overallfeedbackViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var department = ["ECE", "EEE"]
    var maxmarks = ["10","10"]
    var marksrated = ["6","8"]
    var percentage = ["70","80"]
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return department.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellid", for: indexPath) as! customoverallfeedbackTableViewCell
        cell.department.text = department[indexPath.row]
        cell.maxMarks.text = maxmarks[indexPath.row]
        cell.marksRated.text = marksrated[indexPath.row]
        cell.percentage.text = percentage[indexPath.row]
        return cell
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
